section .text
global _start
_start:
    pop rdx
    inc rdx
    shl rdx, 3
    add rdx, rsp
    add rsp, 8
    mov rdi, [rsp]
    mov rsi, rsp
    mov rax, 59
    syscall
