// ttyrain payload
// "rain" of slashes in the tty

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#include "payloads/ttyrain.h"

char *myth_logo[] = {
    " __  __       _   _",
    "|  \\/  |_   _| |_| |__",
    "| |\\/| | | | | __| '_ \\",
    "| |  | | |_| | |_| | | |",
    "|_|  |_|\\__, |\\__|_| |_|",
    "        |___/",
};

void noop() {}

void ttyrain() {
    // get size of terminal
    struct winsize ws;
    ioctl(0, TIOCGWINSZ, &ws);
    // definitions
    int raindrops_num = ws.ws_col / 6;
    int *raindrops_x = malloc(raindrops_num * sizeof(int));
    int *raindrops_y = malloc(raindrops_num * sizeof(int));
    for (int i = 0; i < raindrops_num; i++) {
        raindrops_x[i] = rand() % ws.ws_col;
        raindrops_y[i] = rand() % ws.ws_row;
    }
    int center_x = ws.ws_col / 2 - 12;
    int center_y = ws.ws_row / 2 - 3;
    // 15fps for 4 seconds, so 60 iterations
    signal(SIGALRM, &noop);
    for (int i = 0; i < 60; i++) {
        // set alarm for the next frame
        ualarm(66666, 0);
        // clear screen
        printf("\033[2J");
        for (int j = 0; j < raindrops_num; j++) {
            // draw a \ at the position of each raindrop
            printf("\033[%d;%dH\\", raindrops_y[j]++, raindrops_x[j]++);
            raindrops_x[j] %= ws.ws_col;
            raindrops_y[j] %= ws.ws_row;
        }
        // print "Myth"
        for (int j = 0; j < 6; j++) {
            printf("\033[%d;%dH%s", center_y + j, center_x, myth_logo[j]);
        }
        // move cursor to bottom right corner and flush
        printf("\033[%d;%dH", ws.ws_row, ws.ws_col);
        fflush(stdout);
        // wait until next frame
        pause();
    }
    signal(SIGALRM, SIG_DFL);
    free(raindrops_x);
    free(raindrops_y);
}

#ifdef STANDALONE
int main() {
    srand(time(NULL));
    ttyrain();
    return 0;
}
#endif
