**NOTE:** I am not doing this maliciously, I'm just doing this for learning
things. And for fun too.

Also, I'm not responsible for any damage caused by this thing.

# Myth

Myth is a crappy virus for Linux operating systems. Won't work in any security
focused environment, but is guaranteed to work in a stock Ubuntu install.

Note that Myth has a pretty annoying habit of making systems unbootable.

## How to build

You'll need: `gcc`, `nasm`, `ld`, `xxd`, `strip`, `python`, and of course,
`make`.

Then, just run `make` and it'll create a file called `myth` in the current
directory.

## How to use

For ease of testing, there is a Dockerfile to setup a basic environment where
Myth can run.

In order to do the privilege escalation, you need to run a sudo command before
running Myth.

## How it works

First, to obtain root during the initial infection, it relies on sudo; if sudo
was ran before, then it'll automatically grant root to Myth because it saves
authorizations for a few minutes.

During the initial infection, Myth will drop a backdoor at `/bin/.r`; this
backdoor executes its arguments as root, and is used for privilege escalation.

When infecting a file, Myth will:

- Encrypt the original file with a simple Xorshift-based stream cipher
- Copy itself to the first 32K of the file
- Append the encrypted original file to the file

Determining if a file is infected by Myth is pretty easy: if the file is over
32K and contains `myth` in the EI\_PAD section of the ELF header, it's most
probably infected.

An exception is `/bin/.r`, which has the signature but doesn't actually contain
Myth.

Myth will also start a daemon, mythd, which will try to infect all files
accessible in the PATH every 10 seconds. mythd will write a file in
`/var/run/.myth` containing a path, which the infected files will check; if the
path doesn't exist or `/var/run/.myth` doesn't exist, it will spin up a new
instance of mythd.

Note that mythd appears as a process with no commandline in `ps`.

## Payloads

### ttyrain

ttyrain shows a "rain" of slashes lasting for 4 seconds (15 FPS) and also shows
"Myth" written at the center of the screen.

It has a 1/70 chance of running when running any executable
(including init :)).
