# Myth sign script
# signs the executable

import sys

VERSION = int(open("version.h").read())

data = list(open(sys.argv[1], "rb").read())
data[0x9:0xD] = list(b"myth")
data[0xD] = VERSION & 255
data[0xE] = (VERSION >> 8) & 255
data[0xF] = VERSION >> 16
open(sys.argv[1], "wb").write(bytes(data))
