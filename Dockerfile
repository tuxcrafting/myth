# setup a basic env needed for privilege escalation by Myth

FROM ubuntu:latest

# sudo is required for privilege escalation
RUN apt update
RUN apt install sudo
RUN echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers
# add an user
RUN groupadd wheel
RUN groupadd user
RUN useradd -g user -G wheel -m -s /bin/bash user
RUN echo user:password | chpasswd
# useful packages
RUN yes | apt install xxd
RUN yes | apt install file
RUN yes | apt install lsof
RUN yes | apt install valgrind
# put myth in the user home dir
WORKDIR /home/user
ADD ./myth /home/user
