CC=gcc
CFLAGS=-Isrc -I.
CCLDFLAGS=
LD=ld
LDFLAGS=--nostdlib
ASM=nasm
ASMFLAGS=
OBJ=obj/myth.o obj/mythd.o obj/infector.o obj/xorshift.o \
    obj/payloads/ttyrain.o

myth: $(OBJ)
	$(CC) $^ -o $@ $(CCLDFLAGS)
	strip -s $@
	python bootstrap.py $@

obj/runroot: obj/runroot.o
	$(LD) $^ -o $@ $(LDFLAGS)
	strip -s $@
	python sign.py $@

obj/myth.o: src/myth.c obj/runroot
	xxd -i obj/runroot > src/runroot.h
	$(CC) $< -c -o $@ $(CFLAGS)

obj/%.o: src/%.c
	$(CC) $^ -c -o $@ $(CFLAGS)
obj/%.o: src/%.asm
	$(ASM) $(ASMFLAGS) -f elf64 -o $@ $^
obj/%.bin: src/%.asm
	$(ASM) $(ASMFLAGS) -f bin -o $@ $^

payloads/%: src/payloads/%.c
	$(CC) $^ -o obj/$@ $(CCFLAGS) $(CCLDFLAGS) -DSTANDALONE

.PHONY: docker run

docker: Dockerfile myth
	docker build -t mythdocker .

run:
	docker run -it mythdocker
